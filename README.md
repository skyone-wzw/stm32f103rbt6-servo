# STM32 控制舵机

本仓库是STM32固件库实现的通过串口命令控制舵机转动。

MCU是STM32F103RBT6，舵机信号IO是PIOA_7。

**除了固件库，没有使用或间接使用任何第三方库，包括正点原子的那几个库~~我感觉他们那几个代码写的很烂~~**

用到的外设有：

* 五个定时器中的两个

* 8个共阳LED灯

有两个中断，一个用于控制USART串口，另一个用于控制LED闪烁

----

项目结构

```
F:.
├─.build
│  ├─Listen
│  └─Object
├─DebugConfig
├─include
├─lib
│  ├─FWLib
│  │  ├─include
│  │  └─src
│  ├─led
│  ├─servo
│  ├─std
│  │  ├─include
│  │  └─src
│  ├─timer
│  └─usart
├─mdk
│  ├─Core
│  ├─FWLib
│  │  ├─inc
│  │  └─src
│  └─system
├─src
└─test
```