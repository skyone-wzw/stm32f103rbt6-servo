#pragma once

#include <stm32f10x.h>
#include <led.h>

u8 blink_manager(int flag);
void blink_init();
void TIM2_IRQHandler();
