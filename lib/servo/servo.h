#ifndef __SERVO_H
#define __SERVO_H

#include <stm32f10x.h>

void servo_init();
void servo_set();

#endif
