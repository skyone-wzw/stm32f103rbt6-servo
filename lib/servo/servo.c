#include "servo.h"

static u8 init = 0;

void servo_init() {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitTypeDef pwm;
    pwm.GPIO_Pin = GPIO_Pin_7;
    pwm.GPIO_Mode = GPIO_Mode_AF_PP;
    pwm.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &pwm);
    

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    // 使能通用计时器 3

    TIM_TimeBaseInitTypeDef tim3;
    tim3.TIM_Period = 999;
    // 重装载值为 999, tick 1000 次
    tim3.TIM_Prescaler = 1439;
    // 预分频系数为 1440
    // 系统时钟为72MHz, TIM3 的时钟为 72000000 / 2 * 2 / 1440 = 50kHz
    tim3.TIM_CounterMode = TIM_CounterMode_Up;
    tim3.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM3, &tim3);

    TIM_OCInitTypeDef tim3_oc2;
    tim3_oc2.TIM_OCMode = TIM_OCMode_PWM1;
    tim3_oc2.TIM_OCPolarity = TIM_OCPolarity_High;
    tim3_oc2.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OC2Init(TIM3, &tim3_oc2);

    TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
    TIM_Cmd(TIM3, ENABLE);

    TIM_SetCompare2(TIM3, 25);

    init = 1;
}

void servo_set(int pos) {
    if (init == 1) {
        if (pos > 180)
            pos = 180;
        else if (pos < 0)
            pos = 0;
        TIM_SetCompare2(TIM3, (int)(pos * 100 / 180 + 25));
    }
}
