#include "timer.h"

static u8 each_us = 0;
static u16 each_ms = 0;

/**
  * @brief  初始化定时器
  * @param  None
  * @retval None
  */
void timer_init() {
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
    each_us = SystemCoreClock / 8000000;
    each_ms = each_us * 1000;
}

// 24位寄存器最大储存 0xffffff
// 最大延时为 0xffffff * 8000000 / SYSCLK
// 若`SYSCLK`为72MHz，最大延时为 1864135 us = 1864 ms
// 更长的延时需要分多次延时

/**
  * @brief  以微秒为单位延时
  * @param  us 延时时间，以微秒为单位
  * @retval 无
  */
void delay_us(int32_t us) {
    u32 temp;
    while (us > 0) {
        if (us > MAX_DELAY_TIME_US_EACH) {
            temp = MAX_DELAY_TIME_US_EACH;
        } else {
            temp = us;
        }
        us -= MAX_DELAY_TIME_US_EACH;
        SysTick->LOAD = each_us * temp;
        SysTick->VAL = 0x00;
        SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
        do {
            temp = SysTick->CTRL;
        } while ((temp & 0x01) && !(temp & (1 << 16)));
        SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    }
    SysTick->VAL = 0x00;
}

/**
  * @brief  以毫秒为单位延时
  * @param  ms 延时时间，以毫秒为单位
  * @retval 无
  */
void delay_ms(int32_t ms) {
    u32 temp;
    while (ms > 0) {
        if (ms > MAX_DELAY_TIME_MS_EACH) {
            temp = MAX_DELAY_TIME_MS_EACH;
        } else {
            temp = ms;
        }
        ms -= MAX_DELAY_TIME_MS_EACH;
        SysTick->LOAD = each_ms * temp;
        SysTick->VAL = 0x00;
        SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
        do {
            temp = SysTick->CTRL;
        } while ((temp & 0x01) && !(temp & (1 << 16)));
        SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    }
    SysTick->VAL = 0x00;
}

// 最长等待 59 秒
/**
  * @brief  以毫秒为单位延时
  * @param  func 判断条件是否成立的函数，接收一个`int`型参数：当前等待时间，返回：若返回1，停止计时，若返回0，继续计时
  * @retval 所用时间
  */
uint32_t wait_for(int (*func)(int)) {
    u32 us = 0, temp = 0, each = MAX_DELAY_TIME_US_EACH * each_us;
    u16 count = 0, flg = 1;
    while(flg) {
        SysTick->LOAD = each;   // 延时1,800,000 us
        SysTick->VAL = 0x00;
        SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
        while(flg && (temp & 0x01) && !(temp & (1 << 16))) {    // 如果等待未完成或延时未结束，继续延时
            temp = SysTick->VAL;
            us = (each - SysTick->VAL) / each_us + count * MAX_DELAY_TIME_US_EACH;
            if (func(us))
                flg = 0;
        }
        if (flg == 1) {
            count++;
            us = count * MAX_DELAY_TIME_US_EACH;
        }
    }
    return us;
}
