#ifndef __TIMER_H
#define __TIMER_H

#include <stm32f10x.h>
#define MAX_DELAY_TIME_US_EACH 1800000
#define MAX_DELAY_TIME_MS_EACH 1800

void timer_init();

void delay_ms(int32_t ms);
void delay_us(int32_t us);

uint32_t wait_for(int (*func)(int));

#endif
