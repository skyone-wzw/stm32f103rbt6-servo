#ifndef __LED_H
#define __LED_H

#include <stm32f10x.h>

#define led_on(id)    GPIO_ResetBits(GPIOC, 1 << (id))
#define led_off(id)   GPIO_SetBits(GPIOC, 1 << (id))
#define led_state(id) GPIO_ReadOutputDataBit(GPIOC, 1 << (id))

void led_init(int id);
void led_swap(int id);

#endif
