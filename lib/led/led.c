#include "led.h"

void led_init(int id) {
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    GPIO_InitTypeDef gpio;
    gpio.GPIO_Pin = 1 << id;
    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &gpio);

    GPIO_SetBits(GPIOC, 1 << id);
}

void led_swap(int id) {
    if (led_state(id) == 1) {
        led_on(id);
    } else {
        led_off(id);
    }
}
