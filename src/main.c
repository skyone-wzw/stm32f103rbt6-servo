#include <stdio.h>
#include <stm32f10x.h>
#include <system_stm32f10x.h>
#include <timer.h>
#include <servo.h>
#include <usart.h>
#include "blink.h"

void USART1_IRQHandler() {
    static int pos = 0;
    if(USART_GetITStatus(USART1, USART_IT_RXNE)) {
        char c = USART_ReceiveData(USART1);
        if (c == '\n') {
            if (pos <= 180 && pos >= 0) {
                char str[20];
                sprintf(str, "舵机转到：%d\n", pos);
                for (int i = 0; str[i] != '\0'; i++) {
                    delay_us(100);
                    USART_SendData(USART1, str[i]);
                }
                servo_set(pos);
                blink_manager(1);
            } else {
                char str[] = "只能是0~180之间的数字\n";
                for (int i = 0; str[i] != '\0'; i++) {
                    delay_us(100);
                    USART_SendData(USART1, str[i]);
                }
            }
            pos = 0;
        } else if (c != '\r') {
            if (c <= '9' && c >= '0') pos = pos * 10 + c - '0';
            else return;
        }
        USART_SendData(USART1, c);
    }
}

int main() {
#ifdef VSCode
    SetSysClock();
    // VSCode需要手动初始化系统时钟为 72MHz
#endif
    NVIC_SetPriorityGrouping(NVIC_PriorityGroup_2);
    // 中断优先级分组为 2
    timer_init();

    servo_init();

    usart_init();

    for (int i = 0; i < 8; i++)
        led_init(i);

    blink_init();

    while(1) {
        if (blink_manager(-1) == 1) {
            for (int i = 0; i < 8; i++)
                led_off(i);
            led_on(0);
            delay_ms(50);
            for (int i = 0; i < 7; i++) {
                led_off(i);
                led_on(i + 1);
                delay_ms(50);
            }
            led_off(7);
            blink_manager(0);
        }
    }
}
