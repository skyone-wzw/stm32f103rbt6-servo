#include "blink.h"

static u8 is_set = 0;

u8 blink_manager(int flag) {
    if (flag == 1 || flag == 0)
        is_set = flag;
    else
        return is_set;
    return 0;
}

void blink_init() {
    /*
     * 初始化通用计时器2
     * init_tim2 {
     */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    // 使能通用计时器 2

    TIM_TimeBaseInitTypeDef tim2;
    tim2.TIM_Period = 9999;
    // 重装载值为 9999, tick 10000 次
    tim2.TIM_Prescaler = 7199;
    // 预分频系数为 7200
    // 系统时钟为72MHz, TIM2 的时钟为 72000000 / 2 * 2 / 7200 = 10kHz
    // 即一次 0.1ms
    tim2.TIM_CounterMode = TIM_CounterMode_Up;
    tim2.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM2, &tim2);

    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
    // 配置更新中断

    NVIC_InitTypeDef nvic_tim2;
    nvic_tim2.NVIC_IRQChannel = TIM2_IRQn;
    nvic_tim2.NVIC_IRQChannelPreemptionPriority = 2;
    nvic_tim2.NVIC_IRQChannelSubPriority = 2;
    nvic_tim2.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic_tim2);
    // 设置中断优先级

    TIM_Cmd(TIM2, ENABLE);
    // 使能定时器
    /* 
     * 初始化通用计时器2
     * }
     */
}

void TIM2_IRQHandler() {
    if (TIM_GetFlagStatus(TIM2, TIM_FLAG_Update) == SET) {
        // 判断发生了 `TIM_FLAG_Update` 中断
        if (blink_manager(-1) == 0) {
            // 翻转LED状态
            for (int i = 0; i < 8; i++)
                led_swap(i);
        }
        // 取消中断标志位
        TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
    }
}
